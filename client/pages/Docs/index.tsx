import React from "react";
import BrowserRouter from "react-router-dom";

const { Link } = BrowserRouter as any;

type TodoProps = {};

export const Docs = () => {
  return (
    <div className="px-4 sm:px-6 md:px-8 mb-10 sm:mb-16 md:mb-20">
      <h2 className="sm:text-lg sm:leading-snug font-semibold tracking-wide uppercase mb-3">
        Build anything
      </h2>
      <p className="text-3xl sm:text-5xl lg:text-6xl leading-none font-extrabold tracking-tight mb-8">
        Build whatever you want seriously.
      </p>
      <p className="max-w-4xl text-lg sm:text-2xl font-medium sm:leading-10 space-y-6 mb-6">
        Because Tailwind is so low-level, it never encourages you to design the
        same site twice. Even with the same color palette and sizing scale, it's
        easy to build the same component with a completely different look in the
        next project.
      </p>
      <Link
        className="inline-flex text-lg sm:text-2xl font-medium transition-colors duration-200 focus:ring-2 focus:ring-offset-2 focus:ring-current focus:outline-none rounded-md text-orange-600 hover:text-orange-800"
        to="/"
      >
        Back to Homepage
      </Link>
    </div>
  );
};
