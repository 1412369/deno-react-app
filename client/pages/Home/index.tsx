import React from "react";
import BrowserRouter from "react-router-dom";

const { Link } = BrowserRouter as any;

declare global {
  namespace JSX {
    interface IntrinsicElements {
      [key: string]: any;
    }
  }
}
type HomeProps = {};

export const Home = () => {
  const [text, setText] = (React as any).useState("hello");
  return (
    <div className="px-4 sm:px-6 md:px-8 mb-14 sm:mb-20 xl:mb-8">
      <h1 className="text-4xl sm:text-6xl lg:text-7xl leading-none font-extrabold tracking-tight text-gray-900 mt-10 mb-8 sm:mt-14 sm:mb-10">
        Xây dựng ứng dụng React với Server side rendering bằng Deno
      </h1>
      <p
        style={{ maxWidth: 700, padding: 16, marginTop: 32 }}
        className="w-full flex-none -ml-full rounded-3xl transform shadow-lg bg-gradient-to-br from-orange-400 to-pink-600 -rotate-1 sm:-rotate-2"
      >
        <code className="font-mono text-gray-900 font-bold ">
          Trải nghiệm cũng khá thích á, các bạn vào playground thử nha.
        </code>{" "}
        <code className="font-mono text-gray-900 font-bold ">
          Kết hợp với tailwind nữa thì khỏi cần phải install thêm package gì lằn
          nhằn, code tsx và chạy trực tiếp luôn.
        </code>{" "}
      </p>
      <div
        className="flex flex-wrap space-y-4 sm:space-y-0 sm:space-x-4 text-center"
        style={{ marginTop: 64 }}
      >
        <Link
          className="w-full sm:w-auto flex-none bg-gray-900 hover:bg-gray-700 text-white text-lg leading-6 font-semibold py-3 px-6 border border-transparent rounded-xl focus:ring-2 focus:ring-offset-2 focus:ring-offset-white focus:ring-gray-900 focus:outline-none transition-colors duration-200"
          to="/docs"
        >
          Get started
        </Link>
      </div>
    </div>
  );
};
