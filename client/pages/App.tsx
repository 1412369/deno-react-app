import RouterDOM from "react-router-dom";
import React from "react";
import { Home } from "./Home/index.tsx";
import { Docs } from "./Docs/index.tsx";
const { Switch, Route } = RouterDOM as any;
export const App = () => {
  return (
    <Switch>
      <Route path="/" exact>
        <Home />
      </Route>
      <Route path="/docs">
        <Docs />
      </Route>
    </Switch>
  );
};
