import React from "react";
import ReactDOM from "react-dom";
import RouterDOM from "react-router-dom";
import { App } from "./pages/App.tsx";

const { BrowserRouter } = RouterDOM as any;
declare global {
  var __INITIAL_STATE__: any;
}
(ReactDOM as any).hydrate(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  //@ts-ignore
  document.getElementById("root")
);
