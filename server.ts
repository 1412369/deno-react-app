import { Application } from "oak";
import { green, cyan } from "fmt/colors.ts";
import initRouters from "/server/routes/index.ts";

const app = new Application();
app.use((ctx, next) => {
  console.log(`receive request:${cyan(ctx.request.url.toString())}`);
  next();
});
initRouters(app);

console.log(green("server is running at 8000"));
await app.listen({ port: 8000 });
