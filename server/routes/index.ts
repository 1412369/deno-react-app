import { Application } from "oak";

import HomeRoute from "./home.ts";

export default function initRouters(app: Application) {
  new HomeRoute(app);
}
