import { Router, Application, Context } from "oak";
import reactDomServer from "react-dom-server";
import ReactRouter from "react-router-dom";
import React from "react";
import { App } from "/client/pages/App.tsx";
const { StaticRouter } = ReactRouter as any;

const jsBundlePath = "/main.js";

const { diagnostics, files } = await Deno.emit("./client/index.tsx", {
  bundle: "module",
  compilerOptions: { lib: ["dom", "dom.iterable", "esnext"] },
  importMapPath: "./import_map.json",
});
console.log(diagnostics);
export default class HomeRouter extends Router {
  constructor(app: Application) {
    super();
    this.get("/", this.renderApp).get(jsBundlePath, (context) => {
      context.response.type = "application/javascript";
      context.response.body = files["deno:///bundle.js"];
    });
    this.get("/home", this.renderApp);
    this.get("/docs", this.renderApp);
    app.use(this.routes());
    app.use(this.allowedMethods());
  }
  renderApp = (ctx: Context) => {
    ctx.response.type = "text/html";
    const html = (<any>reactDomServer).renderToString(
      (<any>React).createElement(StaticRouter, { url: ctx.request.url }, [
        (<any>React).createElement(App),
      ])
    );
    ctx.response.body = `<!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">
      <title>Welcome to deno react app</title>
      <style>
      .from-orange-400 {
        --tw-gradient-from: #fb923c;
        --tw-gradient-stops: var(--tw-gradient-from),var(--tw-gradient-to,rgba(251,146,60,0))
      }
      .to-pink-600 {
        --tw-gradient-to: #db2777
      }
      </style>
    </head>
    <body >
      <div id="root">${html}</div>
      <script src="${jsBundlePath}"></script>
    </body>
    </html>`;
  };
}
